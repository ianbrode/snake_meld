import {
  START_GAME, END_GAME,
  SET_HEALTH,
  INC_TIMER,
  RESET,
  INC_BUG
} from './actions';
import { combineReducers } from 'redux'

const gameDefaultState = {
  result: null,
  status: 'NOT_STARTED'
};

const game = (state = gameDefaultState, action) => {
  switch (action.type) {
    case RESET:
    case START_GAME:
      return Object.assign({}, state, {
        result: null,
        status: 'STARTED'
      });
    case END_GAME:
      return Object.assign({}, state, {
        result: action.result,
        status: 'ENDED'
      });
    default:
      return state;
  }
}

const players = (state = 1, action) => {
  switch (action.type) {
    case RESET:
    case START_GAME:
      return 1;
    case END_GAME:
      return action.players;
    default:
      return state;
  }
}

export default combineReducers({
  game,
  players
});
