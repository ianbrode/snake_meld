import React, { Component } from 'react';
import botton from './assets/more.png';;
import SocialBlock from './SocialBlock';
import Howler from 'howler';
import './GoodEnding.css';
import gameSound from './sound/win.wav';
import light from './assets/lights.png';
import winText from './assets/win_text.png';

class GoodEnding extends Component {
  componentDidMount() {
    this.melody = new Howler.Howl({
      src: [gameSound],
      autoplay: true,
      format: 'wav',
      buffer: false,
      loop: true,
      volume: 0.3
    });
  }
  render() {
    const desk = `Я накормил мельдонием змейку`;
    return (<div className='GoodEnd'>
      <img src={winText} className='GoodEndWinText' />
      <img src={light} className='lights' />
      <img src={botton} className='GoodEndButton' alt="Logo" onClick={this.props.reStart}/>
      <SocialBlock
        title='Мельдониевая змейка'
        desk={desk}
        img='https://projects.life.ru/meldonium/share_win.png'
      />

    </div>)
  }

  componentWillUnmount(){
    this.melody.stop();
    this.melody = null;
  }
}

export default GoodEnding;
