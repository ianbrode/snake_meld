import Snake from './snake';

export function sketch (p) {
  var s;
  var scl = 20;

  var food;

  p.setup = function () {
    p.createCanvas(1024, 576);

    s = new Snake(p);
    p.frameRate(10);
    p.pickLocation();
  };

  p.pickLocation = function() {
    var cols = p.floor(p.width/scl);
    var rows = p.floor(p.height/scl);
    food = p.createVector(p.floor(p.random(cols)), p.floor(p.random(rows)));
    food.mult(10);
  }

  p.myCustomRedrawAccordingToNewPropsHandler = function (props) {
    if (s) {
      if (props.direction === 'up') {
        s.dir(0, -1);
      } else if (props.direction === 'down') {
        s.dir(0, 1);
      } else if (props.direction === 'right') {
        s.dir(1, 0);
      } else if (props.direction === 'left') {
        s.dir(-1, 0);
      }
    }

  };

  p.draw = function () {
    p.background(51);
    // p.textSize(32);
    // p.text(text, 10, 30);
    // p.ellipse(150, 150, 8, 8);
    if (s.eat(food)) {
      p.pickLocation();
    }
    s.death();
    s.update();
    s.show();

    p.fill(255, 0, 100);
    p.rect(food.x, food.y, scl, scl);
  };
};
