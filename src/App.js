import React, { Component } from 'react';
import { connect } from 'react-redux';
import { startGame, resetGame, endGame }   from './actions';
import Hammer from 'hammerjs';
import { HotKeys } from 'react-hotkeys';

import Start from './Start';
import BadEnding from './BadEnding';
import GoodEnding from './GoodEnding';
import PlayGround from './PlayGround';

import './App.css';

const keyMap = {
  'moveDown': 'down',
  'moveLeft': 'left',
  'moveRight': 'right',
  'moveUp': 'up'
};

class App extends Component {
  state = {
    direction: 'right'
  }
  handlers = {
    'moveDown': () => {
      if (this.state.direction === 'up') return;
      this.setState({direction: 'down'})
    },
    'moveLeft': () => {
      if (this.state.direction === 'right') return;
      this.setState({direction: 'left'})
    },
    'moveRight': () => {
      if (this.state.direction === 'left') return;
      this.setState({direction: 'right'})
    },
    'moveUp': () => {
      if (this.state.direction === 'down') return;
      this.setState({direction: 'up'})
    },
  };

  componentDidMount() {
    let options = {
      preventDefault: true
    };
    let app = document.querySelector('.App');
    let hammertime = new Hammer(app, options);
    hammertime.get('swipe').set({ direction: Hammer.DIRECTION_ALL });
    hammertime.on('dragup swipeup', (ev) => {
      if (this.state.direction === 'down') return;
      this.setState({direction: 'up'})
    });
    hammertime.on('dragdown swipedown', (ev) => {
      if (this.state.direction === 'up') return;
      this.setState({direction: 'down'})
    });
    hammertime.on('dragleft swipeleft', (ev) => {
      if (this.state.direction === 'right') return;
      this.setState({direction: 'left'})
    });
    hammertime.on('dragright swiperight', (ev) => {
      if (this.state.direction === 'left') return;
      this.setState({direction: 'right'})
    });
  }
  render() {
    const { gameStart, result, status, reset, end, players } = this.props;

    const endWrapper = (a) => {
      this.setState({direction: 'right'})
      end(a);
    }

    let view = null;

    if (status === 'NOT_STARTED') view = <Start gameStart={gameStart} />;

    if (status === 'STARTED') {
      view = <PlayGround
        end={endWrapper}
        direction={this.state.direction}
      />;
    }

    if (status === 'ENDED' && result === 'LOSE') view = <BadEnding reStart={reset} players={players} />;

    if (status === 'ENDED' && result === 'WIN') view = <GoodEnding reStart={reset} />;

    return (
      <HotKeys keyMap={keyMap} handlers={this.handlers} focused={true} attach={window}>
        <div className="App">
          { view }
        </div>
      </HotKeys>
    );
  }
}

export default connect(
  state => ({
    result: state.game.result,
    status: state.game.status,
    players: state.players
  }),
  dispatch => ({
    gameStart: () => dispatch(startGame()),
    reset: () => dispatch(resetGame()),
    end: (e, p) => dispatch(endGame(e, p))
  })
)(App);
