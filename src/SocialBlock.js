import React, { Component } from 'react';
import './SocialButtons.css';


class SocialButtons extends Component {
  render () {
    const fbLink = 'https://www.facebook.com/dialog/feed?app_id=1213833632049097' +
      '&redirect_uri=https://life.ru/990240' +
      '&link=https://life.ru/990240' +
      '&name=' + encodeURIComponent(this.props.title) +
      '&href=https://life.ru/990240' +
      '&picture=' + this.props.img +
      '&caption=' + encodeURIComponent(this.props.desk) +
      '&description=' + encodeURIComponent(this.props.desk);

    const vkLink = 'https://vk.com/share.php?' +
      'url=https://life.ru/990240' +
      '&title=' + encodeURIComponent(this.props.title) +
      '&description=.' + encodeURIComponent(this.props.desk) +
      '&image=' + this.props.img;

    const mateLink = 'https://connect.ok.ru/dk?st.cmd=WidgetSharePreview' +
      '&st.shareUrl=https://life.ru/990240' +
      '&st.title=' + encodeURIComponent(this.props.title) +
      '&st.description='+ encodeURIComponent(this.props.desk);

    return (
      <div className='SocialBlock likely'>
        <a target="_blank" href={vkLink} style={{textDecoration: 'none'}}>
          <div className='VkShare SocBut vkontakte'><span>Поделиться</span></div>
        </a>
        <a target="_blank" href={mateLink} style={{textDecoration: 'none'}}>
          <div className='MatesShare SocBut odnoklassniki'><span>Поделиться</span></div>
        </a>
        <a target="_blank" href={fbLink} style={{textDecoration: 'none'}}>
          <div className='FbShare SocBut facebook'><span>Поделиться</span></div>
        </a>
      </div>
    )
  }
}

export default SocialButtons;
