import React, { Component } from 'react';
import SocialBlock from './SocialBlock';
import Howler from 'howler';
import './BadEnding.css';
import botton from './assets/more.png';
import gameSound from './sound/lose.wav';

class BadEnding extends Component {
  componentDidMount() {
    this.melody = new Howler.Howl({
      src: [gameSound],
      autoplay: true,
      format: 'wav',
      buffer: false,
      loop: true,
      volume: 0.3
    });
  }
  render() {
    const { players } = this.props;
    const desk = `Я собрал ${players} футболистов сборной, но не смог насытить мельдонием змейку`;
    return (<div className='BadEnd'>
      <img src={botton} className='BadEndButton' alt="Logo" onClick={this.props.reStart}/>
      <SocialBlock
        title='Мельдониевая змейка'
        desk={desk}
        img='https://projects.life.ru/meldonium/share_loose.png'
      />
    </div>)
  }

  componentWillUnmount(){
    this.melody.stop();
    this.melody = null;
  }
}

export default BadEnding;
