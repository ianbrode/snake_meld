import React, { Component } from 'react';
import Howler from 'howler';
import P5Wrapper from 'react-p5-wrapper';
import Snake from './snake';
import back from './assets/back.png';

import gameSound from './sound/game.wav';
import throwSound1 from './sound/eat1.wav';
import throwSound2 from './sound/eat2.wav';

import cup1 from './assets/11.png';
import drug1 from './assets/12.png';
import drug2 from './assets/13.png';
import drug3 from './assets/14.png';
import drug4 from './assets/15.png';

import head_back from './assets/16.png';

import head1 from './assets/heads/1.png';
import head2 from './assets/heads/1_1.png';
import head3 from './assets/heads/2.png';
import head4 from './assets/heads/3.png';
import head5 from './assets/heads/4.png';
import head6 from './assets/heads/5.png';
import head7 from './assets/heads/6.png';

import head8 from './assets/heads/7.png';
import head9 from './assets/heads/8.png';
import head10 from './assets/heads/9.png';
import head11 from './assets/heads/10.png';
import head12 from './assets/heads/11.png';
import head13 from './assets/heads/12.png';
import head14 from './assets/heads/13.png';

import head15 from './assets/heads/14.png';
import head16 from './assets/heads/15.png';
import head17 from './assets/heads/16.png';
import head18 from './assets/heads/17.png';
import head19 from './assets/heads/18.png';
import head20 from './assets/heads/19.png';
import head21 from './assets/heads/20.png';

import head22 from './assets/heads/21.png';
import head23 from './assets/heads/22.png';
import head24 from './assets/heads/23.png';

const throw1 = new Howler.Howl({
  src: [throwSound1]
});

const throw2 = new Howler.Howl({
  src: [throwSound2]
});

const timerWrap = (t) => {
  let result = 0;
  let interval = setInterval(() => {
    result++;
  }, t)
  return {
    getTime: () => result,
    off: () => clearInterval(interval)
  }
}

export function sketch (p) {
  let s;
  let scl = 100;
  let bg;
  let food;
  let drugs = [];
  let heads = [];
  let curDrug;
  let cupIcon;
  let end;
  let headBack;
  let tint = 255;
  let cup = false;
  let timer;
  let curInterval = 1;

  const killGame = (a) => { end(a) }
  const getDrug = () => drugs[p.floor(p.random(drugs.length - 1))];

  p.preload = () => {
    bg = p.loadImage(back);
    cupIcon = p.loadImage(cup1);
    headBack = p.loadImage(head_back);

    drugs.push(
      p.loadImage(drug1),
      p.loadImage(drug2),
      p.loadImage(drug3),
      p.loadImage(drug4),
    );

    heads.push(
      p.loadImage(head1),
      p.loadImage(head5),
      p.loadImage(head3),
      p.loadImage(head9),
      p.loadImage(head13),
      p.loadImage(head15),
      p.loadImage(head8),
      p.loadImage(head11),
      p.loadImage(head20),
      p.loadImage(head12),
      p.loadImage(head14),
      p.loadImage(head19),
      p.loadImage(head4),
      p.loadImage(head7),
      p.loadImage(head22),
      p.loadImage(head18),
      p.loadImage(head21),
      p.loadImage(head16),
      p.loadImage(head17),
      p.loadImage(head6),
      p.loadImage(head23),
      p.loadImage(head24),
      p.loadImage(head10),
      p.loadImage(head2)
    )
  }

  p.setup = () => {
    p.createCanvas(1260, 710);
    s = null;
    s = new Snake(p, heads, killGame);
    p.frameRate(2);
    timer = timerWrap(10000);
    curDrug = getDrug();
    p.pickLocation();
  };

  p.pickLocation = () => {
    let cols = 10;
    let rows = 7;
    let vecX = 130 + (p.floor(p.random(cols)) * scl);
    let vecY = 5 + (p.floor(p.random(rows))* scl);

    let inTail = s.tail.some((t) => t.x === vecX && t.y === vecY) || (s.x === vecX && s.y === vecY);

    if (inTail) {
      p.pickLocation();
    } else {
      food = p.createVector(vecX, vecY);
    }
  }

  p.myCustomRedrawAccordingToNewPropsHandler = (props) => {
    if (s) {
      if (props.direction === 'up') {
        if (s.direction === 'down') {
          s.dir(0, 1);
        } else {
          s.dir(0, -1);
          s.direction = 'up'
        }
      } else if (props.direction === 'down') {
        if (s.direction === 'up') {
          s.dir(0, -1);
        } else {
          s.dir(0, 1);
          s.direction = 'down'
        }
      } else if (props.direction === 'right') {
        if (s.direction === 'left') {
          s.dir(-1, -0);
        } else {
          s.dir(1, 0);
          s.direction = 'right'
        }
      } else if (props.direction === 'left') {
        if (s.direction === 'right') {
          s.dir(1, -0);
        } else {
          s.dir(-1, 0);
          s.direction = 'left'
        }
      }
    }

    end = props.end;
  };

  p.draw = () => {
    p.background(bg);
    p.noStroke();

    if (s.eat(food)) {
      p.round(p.random) ? throw1.play() : throw2.play();
      if (s.total === 10) p.frameRate(2);
      if (s.total === 20) p.frameRate(3);
      if (s.total === 24) {
        killGame(1, s.total);
        p.noLoop();
      }
      if (s.total === 23) {
        cup = true;
        timer = timerWrap(15000);
        curInterval = 1;
        curDrug = cupIcon;
        tint = 255;
        p.pickLocation();
      }
    } else {
      if (timer.getTime() == curInterval) {
        if (cup) {
          killGame(0, s.total);
          p.noLoop();
        }
        tint = 255;
        p.pickLocation();
        curDrug = getDrug();
        curInterval = curInterval + 1;
      }
      p.tint(255, (tint < 100 ? 100 : tint));
      p.image(curDrug, food.x, food.y, 100, 100);
      p.noTint();
      tint = cup ? tint - 10 : tint - 50;
    }
    s.death();
    s.update();
    s.show();
  };
};

class PlayGround extends Component {
  componentDidMount() {
    this.melody = new Howler.Howl({
      src: [gameSound],
      autoplay: true,
      format: 'wav',
      buffer: false,
      loop: true,
      volume: 0.3
    });
  }

  componentWillUnmount() {
    this.melody.stop();
    this.melody = null;
  }

  render() {
    const {end, direction} = this.props;
    return (
      <div>
        <div
          id="p5_loading"
          className="loadingclass"
        >
          Загрузка
        </div>
        <P5Wrapper
          sketch={sketch}
          direction={direction}
          end={end}
        />
      </div>
    )
  }
};

export default PlayGround;
