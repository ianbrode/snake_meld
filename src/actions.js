export const START_GAME   = 'START_GAME';
export const END_GAME     = 'END_GAME';
export const SET_HEALTH   = 'SET_HEALTH';
export const INC_TIMER    = 'INC_TIMER';
export const RESET        = 'RESET';
export const INC_BUG      = 'INC_BUG';

const results = ['LOSE', 'WIN'];
let timer     = null;

export const startGame = () => ({
  type: START_GAME,
  result: null
});

export const endGame = (result, players = 1) => ({
  type: END_GAME,
  result: results[result],
  players
});

export const setHeath = (health) => ({
  type: SET_HEALTH,
  health
});

const stop = () => { clearInterval(timer) };

const tick = () => ({ type: INC_TIMER });

export const killBug = () => ({ type: INC_BUG });

const startTimer = () => (dispatch, getState) => {
  stop();

  timer = setInterval(() => {
    let { time } = getState();

    if (time <= 0){
      stop();
      dispatch(endGame(1));
      return;
    }

    dispatch(tick())
  }, 1000);
};

export const initGame = () => (dispatch, getState) => {
  dispatch(startGame());
  dispatch(startTimer());
};

export const loseHealth = () => (dispatch, getState) => {
  const { user } = getState();
  const health = user.health - 1;
  dispatch(setHeath(health));
  if (health < 0) {
    stop();
    dispatch(endGame(0));
  }
};

export const resetGame = () => dispatch => {
  dispatch({type: RESET});
};
