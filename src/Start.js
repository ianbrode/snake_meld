import React from 'react';
import glass from './assets/play.png';
import cock from './assets/main/cock.png';
import header from './assets/main/header.png';

const style = {
  position: 'absolute',
  bottom: '7vw',
  left: '54vw',
  cursor: 'pointer',
  width: '23vw',
  height: '17vw'
}

const cockStyle = {
  position: 'absolute',
  bottom: '4vw',
  left: '9vw',
  width: '29vw',
  height: '46vw'
}

const headerStyle = {
  position: 'absolute',
  bottom: '29vw',
  left: '42vw',
  width: '46vw',
  height: '17vw'
}

const Start = ({gameStart}) => (
  <div className='Start'>
    <img src={cock} style={cockStyle} alt="Logo"/>
    <img src={header} style={headerStyle} alt="Logo"/>
    <img src={glass} onClick={gameStart} style={style} alt="Logo"/>
  </div>
);

export default Start;
