export default function Snake(p, heads, kill) {
  this.x = 30;
  this.y = 5;
  this.xspeed = 1;
  this.yspeed = 0;
  this.total = 0;
  this.tail = [];
  this.direction = 'left';
  var scl = 100;

  this.eat = function(pos) {
    var d = p.dist(this.x, this.y, pos.x, pos.y);
    if (d < 1) {
      this.total++;
      return true;
    } else {
      return false;
    }
  }

  this.dir = function(x, y) {
    this.xspeed = x;
    this.yspeed = y;
  }

  this.death = function() {
    for (var i = 0; i < this.tail.length; i++) {
      var pos = this.tail[i];
      var d = p.dist(this.x, this.y, pos.x, pos.y);
      if (d < 1) {
        this.total = 0;
        this.tail = [];
        kill(0, this.total);
        p.noLoop();
      }
    }
  }

  this.update = function() {
    let x;
    let y;

    if (this.total === this.tail.length) {
      for (var i = 0; i < this.tail.length - 1; i++) {
        this.tail[i] = this.tail[i + 1];
      }
    }
    this.tail[this.total - 1] = p.createVector(this.x, this.y);

    this.x = this.x + this.xspeed * scl;
    this.y = this.y + this.yspeed * scl;

    x = this.x;
    y = this.y;

    this.x = p.constrain(this.x, 130, p.width - 230);
    this.y = p.constrain(this.y, 5, p.height - 105);
    if (x !== this.x || y !== this.y) {
      this.total = 0;
      this.tail = [];
      kill(0, this.total);
      p.noLoop();
    }
  }

  this.show = function() {
    p.fill('rgba(0,0,0,0.7)');
    for (var i = 0; i < this.tail.length; i++) {
      p.rect(this.tail[i].x, this.tail[i].y, scl, scl);
      if (this.tail.length-i <= 23) {
        p.image(heads[this.tail.length-i], this.tail[i].x, this.tail[i].y, scl, scl);
      }
    }
    p.rect(this.x, this.y, scl, scl);
    p.image(heads[0], this.x, this.y, scl, scl);
  }
}
